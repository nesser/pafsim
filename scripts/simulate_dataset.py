"""This CLI programm starts a simulation by the passed configuration
"""

import json
from argparse import ArgumentParser

from pafsim import Executor


def main_cli():
    parser = ArgumentParser(description="Script for simulating PAF backend processing networks")
    parser.add_argument('--config','-c', dest='config', type=str, default='example/beamformer_test.json',
                        help='The configuration file describing the PAF backend networks')
    conf = parser.parse_args().config
    with open(conf, "r") as fp:
        jconf = json.load(fp)
    exec = Executor(jconf)
    exec.run()

if __name__ == "__main__":
    main_cli()
