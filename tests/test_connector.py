import unittest
from pafsim.connector import Connector, EdgeConnectionError
import pafsim.processor as pp

class TEST_Connector(unittest.TestCase):

    def setUp(self) -> None:
        return super().setUp()

    def tearDown(self) -> None:
        return super().tearDown()

    def test_non_matching_nodes(self):
        proc1 = pp.Correlator("dummy1")
        proc2 = pp.Correlator("dummy2")
        conn = Connector(proc1, proc2)
        with self.assertRaises(EdgeConnectionError):
            conn.connect()

