import unittest
import shutil
import os
import numpy as np
from pafsim.vector import Vector
import pafsim.processor as pp

DIR = "tmp/"

class TEST_Vector(unittest.TestCase):

    def setUp(self) -> None:
        if not os.path.exists(DIR):
            os.makedirs(DIR)
        return super().setUp()

    def tearDown(self) -> None:
        if os.path.exists(DIR):
            shutil.rmtree(DIR)
        return super().tearDown()

    # def test_load_store(self):
    #     proc = pp.Correlator("dummy")
    #     proc.output = np.arange(10)
    #     stored = Vector(proc)
    #     stored.store(DIR)
    #     loaded = Vector()
    #     loaded.load(stored.path)
    #     self.assertEqual(stored.data, loaded.data)
    #     self.assertEqual(stored.order, loaded.order)
    #     self.assertEqual(stored.dtype, loaded.dtype)
    #     self.assertEqual(stored.name, loaded.name)
    #     self.assertEqual(str(stored), str(loaded.name))

