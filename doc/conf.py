
extensions = ['sphinx.ext.autodoc', 'sphinx.ext.coverage',
        'sphinx.ext.napoleon', 'sphinx_rtd_theme',  'recommonmark', 'sphinx.ext.intersphinx',
        "nbsphinx",
        "IPython.sphinxext.ipython_console_highlighting"
        ]
# 'sphinx.ext.viewcode',

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

#AUTOAPI
extensions.append('autoapi.extension')

autoapi_type = 'python'
autoapi_dirs = ["../pafsim", "../scripts"]
autoapi_add_toctree_entry = False
autoapi_options = ["members", "undoc-members", "show-inheritance-diagram", "show-inheritance", "imported-members"]
autoapi_python_use_implicit_namespaces = True
#"show-module-summary"

autoapi_ignore = ['*test*', '*tests*']
autoapi_python_class_content = 'both'
autoapi_keep_files = True

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "sphinx_rtd_theme"

html_theme_options = {
    'collapse_navigation': False,
    'logo_only': True,
    'display_version': False,
}

html_show_sourcelink = False

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

html_js_files = [
    'schema_doc.min.js'
]

html_css_files = [
    'custom.css',
   'schema_doc.css'
]


def setup(app):
    app.add_css_file('custom.css')
