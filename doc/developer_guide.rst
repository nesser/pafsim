Developer Guide
===============

Module Design
-------------

The pafsim module is composed of submodules and follows a object-oriented approach.
The top level view of the module is shown the figure below

.. image:: img/top_level_uml_design.png
   :alt: Alternate text for the image
   :align: center
   :width: 700px
   :height: 350px

The submodule `processing` contains the implemented PAF components. All concrete components (aka processors) are inheriting from the abstract base class `Processor(ABC)`.


Develope Your Own Processor and Algorithms
------------------------------------------

A implementation of a processor is required to inherit from the processing.Processor class. The concret sub-class has to implement the abstract classes of base.




List of ToDo's
--------------

- Performance metric / Evaluation Framework to evaluate and benchmark algorithms, data correctness etc. (based on a classifiers (Tensorflow?))
- Arbitary testvector injection - chunksizing data sets, use of Dask may help
- Implementation and integration of Edge/Connector class (transposer, robustness against shape mismatch by padding)
- RFI mitigation algorithms
- More comprhensive implementation of Processor algorithm (e.g. Power-, Stokes beamformer)
- Synthesis filter
- Different beamforming algorithms
- Avoid using pickle for test vector. A more robust way is needed
- Acclerate algorithms by the use of Cupy.
- Comprehensive unittesting - reach code coverage of minimum 90%


Contribution
------------

We are happy for any contribution, but the contribution needs to follow some rules.

- The API is not allowed to change
- Docstrings and code needs to fulfill PEP standards
- Pushes to the main branch are not possible. Every contribution requires a merge reqeusts which are reviewed by the maintainers.