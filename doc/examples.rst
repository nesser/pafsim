Examples (Jupyter-Notebooks)
============================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   notebooks/example_simulation.ipynb


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   notebooks/frontend_interface.ipynb
