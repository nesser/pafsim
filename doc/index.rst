The Phased Array Feed Backend Simulator (pafsim)
================================================

To get started with PAFSim, simply install the module via pip and explore the detailed documentation and examples provided. Whether you are designing a new PAF backend or optimizing an existing one, PAFSim is your tool for ensuring data correctness and validation.

The pafsim module is developed by the Max-Planck Institute for Radioastronomy and the Radioblocks community.

Disclaimer: The module is currently under construction. A lot of components and features are not implemented yet. The `Developer Guide <developer_guide.html#list-of-todo-s>`_ contains a list of ToDo's and everybody is welcome to `contribute <developer_guide.html#contribute>

Installation
------------

It is recommended to create a virtual environment before installing the package.

.. code-block:: bash

    python -m venv /path/to/venv/

Activate the venv

.. code-block:: bash

    source /path/to/venv/bin/activate`

The project is managed by the `pyproject.toml`-file. Hence, an installation with `pip` is the prefered way.

In the next step we clone the pafsim repository:

.. code-block:: bash

    git clone https://gitlab.mpcdf.mpg.de/nesser/pafsim && cd pafsim

and install it

.. code-block:: bash

    pip install .

All required dependencies are resolved.

.. toctree::
   :maxdepth: 2
   :caption: Links

   introduction.rst
   user_guide.rst
   developer_guide.rst
   examples.rst

`API reference <autoapi/pafsim/index.html#module-pafsim>`_