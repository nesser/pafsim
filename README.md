# Backend Simulation Tool for Phased Array Feeds (PAFs)
This repository contains the `pafsim` python module. The module implements various PAF backend components such as Correlator, Beamforer, Calibrator etc. The purpose of this module is a comprehensive simulation tool for PAF backends which may act like a "golden reference" for real world PAFs.

The PAF Backend Simulator is part of the [Radioblocks]() project. The counterpart to this project is the [PAF Frontend Simulator](https://gitlab.mpcdf.mpg.de/yayian/paffrontendsimpublic). Both the front- and backend simulator share an interface allowing to generate realisitic datasets.

## Install

It is recommended to create a virtual environment before installing the package.

`python -m venv /path/to/venv/`

Activate the venv

`source /path/to/venv/bin/activate`

The project is managed by the `pyproject.toml`-file. Hence, an installation with `pip` is the prefered way.

In the next step we clone the pafsim repository:

`git clone https://gitlab.mpcdf.mpg.de/nesser/pafsim && cd pafsim`

and install it

`pip install .`

All required dependencies are resolved.

## Documentation

The API documentation and user guide can be found [here](https://nesser.pages.mpcdf.de/pafsim/).

Jupyter notebook's on how to use the `pafsim` module are in the `example`-folder.

## Contact
Niclas Esser - <nesser@mpifr-bonn.mpg.de>
