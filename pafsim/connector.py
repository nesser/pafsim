# import numpy as np
from pafsim.processor import Processor

class EdgeConnectionError(Exception):
    """Exception class used for invalid connections
    """

class Connector:
    """The Connector class is used as edges in the ProcessingChain. It transposes output
    data to the expected input format of the post processors.

    ToDo: Currently the ProcessingChain does not use the Connector class
    """
    def __init__(self, a_node: Processor, b_node: Processor):
        self.a_node = a_node
        self.b_node = b_node

    def connect(self):
        """Connects to Processors and checks if the connection is valid

        Raises:
            EdgeConnectionError: If a connection is invalid
        """
        if self.a_node.O_FORMAT in self.b_node.I_FORMAT:
            print("Connection without transpose")
            self.assign()
        elif set(self.a_node.O_FORMAT) in [set(x) for x in self.b_node.I_FORMAT]:
            print("Connection needs transpose")
            self.transpose()
            self.assign()
        else:
            raise EdgeConnectionError("Output format of node A does not match input formats of node B")

    def assign(self):
        """Assigns the Processors

        Raises:
            EdgeConnectionError: Mismatch of inputs
        """
        if self.b_node.N_INPUTS > len(self.b_node.pp):
            raise EdgeConnectionError(f"Can not assign more than {self.b_node.N_INPUTS} connections")
        self.b_node.pp.append(self.a_node)

    def transpose(self):
        """Transpose the output of the pre processor to the expected input of the post processor

        ToDo: Not implemented yet
        """
