import os
import shutil
from typing  import Dict
import networkx as nx
import matplotlib.pyplot as plt

import pafsim.processor as processing
from pafsim.vector import Vector

class ProcessingChain():
    """The ProcessingChain class provides an interface to connect Processor objects together
    by constructing a directed acyclic graph (DAG).
    """
    def __init__(self, conf: dict, name: str=""):
        """Construct a ProcessingChain object

        Args:
            conf (dict): A dictionary that describes the DAG. See 'example/pafsim_config.json' for
                a concrect example.
            name (str, optional): When creating multiple ProcessingChain objects with dependencies,
                a name can be provided to refer to dependend ProcessingChains. Defaults to "".
        """
        self.name: str = name
        self.conf: dict = conf
        self.setup_flag: bool = False
        self.processed: bool = False
        self.dag: nx.DiGraph = nx.DiGraph()
        self.dag_list: list = []
        self.processors: Dict[str,processing.Processor] = {}

    def setup(self):
        """Sets up the DAG by instantiating Processor object and populating the edges and nodes"""
        # Create processors objects and add each as a DAG node
        for pname, pconf in self.conf.items():
            if 'processor' not in pconf:
                continue
            self.dag.add_node(pname)
            self.processors[pname] = processing.createProcessor(pconf['processor'], pname, pconf)

        # Populate DAG graph and sort processor list by DAG
        self.populate()
        self.dag_list = list(nx.topological_sort(self.dag))
        self.setup_flag = True

    def populate(self):
        """Populate the DAG under the use of the networkx-module"""
        # Iterate over existing processors and connect pre-processors
        for pname, proc in self.processors.items():
            if "input" not in proc.conf:
                continue
            if not isinstance(proc.conf["input"], list):
                proc.conf["input"] = [proc.conf["input"]]
            for ppname in proc.conf["input"]:
                self.dag.add_edge(ppname, pname)
                proc.setPreProcessor(self.processors[ppname])

    def addProcessor(self, obj: processing.Processor):
        """Adds a processor object to the Graph. Can be used to construct a Graph from objects
        rather than from configurations (input dictionary). However, this function can be used
        to inject a new Processor to an existing Graph

        Args:
            obj (processing.Processor): The processor object to add
        """
        self.processors[obj.id] = obj
        self.conf.update({str(obj.id):obj.conf})
        self.dag.add_node(obj.id)
        self.populate()

    def getProcessor(self, name: str) -> processing.Processor:
        """Returns a Processor object by its name / ID

        Args:
            name (str): The name of the Processor

        Returns:
            processing.Processor: The Processor if the ID exists in the chain, otherwise returns
                None
        """
        for pname, proc in self.processors.items():
            if pname == name:
                return proc
        return None

    def plot(self, path: str="", figsize: tuple=(8,4)):
        """Plots the DAG

        Args:
            path (str, optional): If not set to "" it stores the plot in the given directory. Defaults to "".
            figsize (tuple, optional): Size of the plotted figure. Defaults to (8,4).
        """
        fig = plt.figure(num = 1, figsize=figsize)
        nx.draw_spectral(self.dag, with_labels=True,
                         node_shape="s",
                         font_weight='bold',
                         node_color='skyblue',
                         node_size=5000,
                         edge_color='gray',
                         linewidths=2,
                         font_size=9)
        fig.suptitle("Directed acyclic graph (DAG)")
        if path:
            fig.savefig(path + 'dag.png')
            plt.close()
        else:
            plt.show()

    def process(self):
        """Starts the processing of all Processors from top to bottom

        Raises:
            Exception: If the ProcessingChain.setup() method was not called before
                ProcessingChain.process()
        """
        if self.setup_flag is False:
            raise Exception("Chain not set, use setup() before process()")
        for pname in self.dag_list:
            self.processors[pname].process()
        self.processed = True

    def plotAll(self, path: str="", figsize: tuple=(8,4)):
        """Calls all plot methods of the processors

        Args:
            path (str, optional): If not set to "" it stores the plot in the given directory. Defaults to "".
            figsize (tuple, optional): Size of the plotted figure. Defaults to (8,4).

        Raises:
            Exception: If the ProcessingChain.process() method was not called before
                ProcessingChain.plotAll()
        """
        if self.processed is False:
            raise Exception("Chain not processed, use process() before plot()")
        for pname in self.dag_list:
            self.processors[pname].plot(path, figsize)

    def save(self, plot: bool=True, overwrite:bool=True):
        """Stores the configurations and marked vectors of the procesing chains in dedicated folders

        Args:
            plot (bool, optional): If set to True, stores the plots of all data. Defaults to True.
            overwrite (bool, optional): If set to True, overwrites existing dataset with the same
                storage location. Defaults to True
        """
        if "location" not in self.conf:
            return
        directory = os.path.abspath(self.conf["location"])

        if overwrite is True and os.path.exists(directory):
            shutil.rmtree(directory)
        if not os.path.exists(directory):
            os.makedirs(directory)

        with open(os.path.join(directory, "conf.json"), "w", encoding="utf-8") as o:
            o.write(str(self.conf))
        os.mkdir(os.path.join(directory, 'data/'))
        if plot:
            os.mkdir(os.path.join(directory, 'plot/'))
            self.plotAll(os.path.join(directory, 'plot/'))

        for tv_name in self.conf["store"]:
            tv = Vector(self.getProcessor(tv_name))
            tv.store(os.path.join(directory, 'data/'))
