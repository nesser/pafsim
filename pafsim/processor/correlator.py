import numpy as np
import matplotlib.pyplot as plt

from pafsim.processor._processor import Processor

class Correlator(Processor):
    """The Correlator takes time series data as input and correlates them.
    The input is a time series of the form APT or FAPT
    The output is are ACMs of the form NFPAA, where N is the number of ACMs
    """
    N_INPUT = 1
    I_FORMAT = [['F', 'A', 'P', 'T']]
    O_FORMAT = ['N', 'F', 'P', 'A', 'A'] # N = number of ACM

    def __init__(self, name: str, **kwargs):
        """Construct a Correlator object

        Args:
            name (str):  The unique name of theCorrelator
        kwargs:
            acc (int): Number of ACMs to create.
            mode (str): 'acm' -> correlate and build covariance matrices (default)
        """
        super().__init__(name, **kwargs)
        self.acc = kwargs.get('acc', 2)

    @property
    def default(self) -> str:
        """Default process function
        """
        return "acm"

    @property
    def shape(self) -> tuple:
        """The shape of the output array

        Returns:
            tuple: The shape
        """
        shape = []
        shape.append(self.acc)
        shape.append(self.pp[0].dim('F'))
        shape.append(self.pp[0].dim('P'))
        shape.append(self.pp[0].dim('A'))
        shape.append(self.pp[0].dim('A'))
        return tuple(shape)

    def acm(self) -> np.ndarray:
        """Processing function to compute the covariances

        Returns:
            np.ndarray: The output array
        """
        nacc = int(self.pp[0].dim('T') // self.acc)
        x = np.asarray(self.pp[0].output).transpose(0,2,1,3)
        o = np.zeros(self.shape, dtype=self.pp[0].output.dtype)
        for n in range(self.dim('N')):
            o[n] = np.matmul(x[..., n*nacc:(n+1)*nacc], np.conj(x[..., n*nacc:(n+1)*nacc]).transpose(0,1,3,2))
        return o

#pylint: disable=R0801
    def plot(self, path="", figsize=(8,4)):
        """Plotting function to plot the amplitude of covariance matrices. Just a sub-set is
        randomly chosen

        Args:
            path (str, optional): If not set to "" it stores the plot in the given directory.
                Defaults to "".
            figsize (tuple, optional): Size of the plotted figure. Defaults to (8,4).
        """
        sub = []
        fig = plt.figure(num=4*4, figsize=figsize)
        channels = np.random.randint(0, self.pp[0].dim('F'), 16)
        for i, f in enumerate(channels):
            sub.append(plt.subplot(4, 4, i + 1))
            sub[i].set_title("Channel " + str(f))
            sub[i].imshow(np.log(np.abs(self.output[0, f, 0])),interpolation='nearest', aspect='auto')
        fig.tight_layout()
        if path:
            fig.savefig(path + self.name + '.png')
            plt.close()
        else:
            plt.show()
#pylint: enable=R0801
