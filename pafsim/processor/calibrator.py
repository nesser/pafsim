import numpy as np
import matplotlib.pyplot as plt
from pafsim.processor._processor import Processor

class WeightGenerator(Processor):
    """The WeightGenerator creates beam weights under the use of ACMs.
    It uses the Correlator as input, the format is FPAA.
    The output array are the frequency dependend weights of the form BFPA.
    """
    N_INPUT = 1
    I_FORMAT = [['N', 'F', 'P', 'A', 'A']] # N = number of ACM
    O_FORMAT = ['B', 'F', 'P', 'A']


    def __init__(self, name: str, **kwargs):
        """Construct a WeightGenerator object

        Args:
            name (str): The unique name of theWeightGenerator
        kwargs:
            beams (int): The number of beams to produce. This parameter is implicitly determined
                when using the maxsnr processing
            mode (str): maxsnr -> maximum signal-to-noise algorithm (default)
        """
        super().__init__(name, **kwargs)
        self.beam = kwargs.get('beams', 2)

    @property
    def default(self) -> str:
        """Default process function
        """
        return "maxsnr"

    @property
    def shape(self) -> tuple:
        """The shape of the output array

        Returns:
            tuple: The shape
        """
        shape = []
        if self.pp[0]:
            shape.append(self.beam)
            shape.append(self.pp[0].dim('F'))
            shape.append(self.pp[0].dim('P'))
            shape.append(self.pp[0].dim('A'))
        else:
            shape = self.output.shape
        return tuple(shape)

    def maxsnr(self) -> np.ndarray:
        """Processing function to compute beam weight under the use of th maximum signal-to-noise
        algorithms

        Returns:
            np.ndarray: _description_
        """
        x = np.asarray(self.pp[0].output)
        acm_sn = np.zeros((self.dim('F'),
                           self.dim('P'),
                           self.dim('A'),
                           self.dim('A')), dtype='complex')
        weights = np.zeros(self.shape, dtype='complex')
        for b in range(self.beam):
            acm_off = x[b*2]
            acm_on = x[b*2+1]
            acm_sn = np.matmul(np.linalg.inv(acm_off), acm_on)
            for fidx in range(self.dim('F')):
                for pidx in range(self.dim('P')):
                    eig_val, eig_vec = np.linalg.eig(acm_sn[fidx, pidx])
                    weights[b, fidx, pidx] = eig_vec[np.argmax(np.abs(eig_val), axis=-1)]#.flatten()
        return weights


    def random(self) -> np.ndarray:
        """Processing function creating random weights

        Returns:
            np.ndarray: The output array
        """
        if np.iscomplex(self.pp[0].dtype):
            return np.random.normal(0.0, scale=1.0, size=self.shape) \
                + 1j*np.random.normal(0.0, scale=1.0, size=self.shape)
        return np.random.normal(0.0, scale=1.0, size=self.shape)

    def zero(self) -> np.ndarray:
        """Processing function creating zero weights

        Returns:
            np.ndarray: The output array
        """
        return np.zeros(self.shape)

    def bypass(self) -> np.ndarray:
        """Processing function to bypass using a diagonal matrix with ones and zeros

        Returns:
            np.ndarray: The output array
        """
        data = np.zeros(self.shape, dtype="complex")
        for p in range(self.dim('P')):
            for a in range(self.dim('A')):
                data[...,p,a] = 1 + 0j
        return data

#pylint: disable=R0801
    def plot(self, path="", figsize=(8,4)):
        """Plotting function to plot the amplitude of the beamweight

        Args:
            path (str, optional): If not set to "" it stores the plot in the given directory. Defaults to "".
            figsize (tuple, optional): Size of the plotted figure. Defaults to (8,4).
        """
        sub = []
        fig = plt.figure(num=self.dim('B')*self.dim('P'), figsize=figsize)
        for b in range(self.dim('B')):
            for p in range(self.dim('P')):
                sub.append(plt.subplot(self.dim('B'), self.dim('P'), b * self.dim('P') + p + 1))
                sub[b*self.dim('P')+p].set_title("Beam " +str(b) + "." + str(p))
                sub[b*self.dim('P')+p].set_xlabel("Element")
                sub[b*self.dim('P')+p].set_ylabel("Channel")
                sub[b*self.dim('P')+p].imshow(np.log(np.abs(self.output[b, :, p])),interpolation='nearest', aspect='auto')
        fig.tight_layout()
        if path:
            fig.savefig(path + self.name + '.png')
            plt.close()
        else:
            plt.show()
#pylint: enable=R0801
