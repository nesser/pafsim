import os
import typing as ty
from matplotlib import pyplot as plt
import h5py as h5
import numpy as np

from pafsim.processor._processor import Processor
class FrontendSimFile(h5.File):
    """
    The FrontendSimFile is class to read ACMs and properties from an HDF5 file produced
    by the PAF Frontend Simulator. The class inherits from the h5py.File class.
    """
    signal_base_path = "acms/signal/{name}/"
    noise_base_path = "acms/noise/{name}/sys"
    rfi_base_path = "acms/rfi/{name}"

    def __init__(self, path: str):
        """Construct an FrontendSimFile object. File is only opened in read-mode

        Args:
            path (str): Path to the HDF5 file
        """
        super().__init__(path, "r")

    def view(self) -> str:
        '''
        Get structure tree of the hdf5 file

        Returns:
            str: Representation of the structure tree
        '''
        def print_attrs(name, obj):
            nonlocal string_pointer
            level = name.count('/')
            if level > 0:
                shift = (level - 1) * '  ' + '└' + '─'
            else:
                shift = ''
            item_name = name.split("/")[-1]
            if hasattr(obj, 'nbytes'):
                size_string = f' ({str(obj.size)})'
            else:
                size_string = ''
            string_pointer += shift + item_name + size_string + '\n'
            if hasattr(obj.attrs, 'items'):
                for key, val in obj.attrs.items():
                    string_pointer += '  ' + shift + f"{key}: {val}" + '\n'
        string_pointer = ""
        self.visititems(print_attrs)
        return string_pointer

    @property
    def nelements(self) -> int:
        """Returns the number of elements simulated in the PAF

        Returns:
            int: Number of elements
        """
        return len(self["array/element_ids"])

    @property
    def npol(self) -> int:
        """Returns the number of elements simulated in the PAF

        Returns:
            int:  Number of polarisations
        """
        return 1#len(set(self["array/element_ids"]))

    def exists(self, path: str) -> bool:
        """Checks is a path in the HDF5 exists

        Args:
            path (str): The path to proof existence

        Returns:
            bool: True if it exists, otherwise false
        """
        if path in self:
            return True
        return False

    def dataset(self, path: str) -> np.ndarray:
        """Returns a dataset by the given path

        Args:
            path (str): The path of the dataset

        Returns:
            np.ndarray: The data set as a numpy array
        """
        if self.exists(path):
            return self[path]
        print(f"Path {path} does not exists in dataset")
        return None

    def datasets(self, base_path: str, names: set, suffix: str="acm") -> ty.List[np.ndarray]:
        """Returns multiple datasets with matching names and suffix within the base path.
            The paths of the datasets is constructed like this:
                base_path/names[0]/suffix
                base_path/names[1]/suffix
                base_path/names[n]/suffix

        Args:
            base_path (str): The base path of the datasets
            names (set): A set or list of names / sub directories
            suffix (str, optional): The data set names. Defaults to "acm".

        Returns:
            ty.List[np.ndarray]: _description_
        """
        acms = []
        for name in names:
            path = os.path.join(base_path.format(name=name), suffix)
            acms.append(self.dataset(path))
        return acms

    def signal(self, names: set, suffix: str="acm") -> ty.List[np.ndarray]:
        """Returns datasets of simulated signals with matching names

        Args:
            names (set): Set or List of names
            suffix (str, optional): The data set names. Defaults to "acm".

        Returns:
            ty.List[np.ndarray]: The datasets, List of numpy arrays
        """
        return self.datasets(self.signal_base_path, names, suffix)

    def noise(self, names: set, suffix: str="acm") -> ty.List[np.ndarray]:
        """Returns datasets of simulated noises with matching names

        Args:
            names (set): Set or List of names
            suffix (str, optional): The data set names. Defaults to "acm".

        Returns:
            ty.List[np.ndarray]: The datasets, List of numpy arrays
        """
        return self.datasets(self.noise_base_path, names, suffix)

    def rfi(self, names: set, suffix: str="acm") -> ty.List[np.ndarray]:
        """Returns datasets of simulated RFI with matching names

        Args:
            names (set): Set or List of names
            suffix (str, optional): The data set names. Defaults to "acm".

        Returns:
            ty.List[np.ndarray]: The datasets, List of numpy arrays
        """
        return self.datasets(self.rfi_base_path, names, suffix)


class Generator(Processor):
    """The Generator class generates wideband time series data for all simulated PAF elements.
        The simulation is based on the read ACMs of the Frontend Simulation.
        The Generator inherits from the Processor class and can be used as the input for a processing chain.
    """
    N_INPUT = 0
    I_FORMAT = None
    O_FORMAT = ['A', 'P', 'T']

    def __init__(self, name: str, **kwargs):
        """Construct a Generator object

        Args:
            name (str): The unique name of theGenerator
        kwargs:
            dataset (str): Path to an HDF5 file containing the output of the PAF Frontend simulator
            noise (str): Names of the noise datasets to use for the time series generation
            signal (str): Names of the signal datasets to use for the time series generation
            rfi (str): Names of the RFI datasets to use for the time series generation
            width (int): The length of the time series to generate
        """
        super().__init__(name, **kwargs)
        self.dataset = self.conf.get("dataset", "example/frontend_simulation_example.h5")
        self.noise = self.conf.get("noise", [0])
        self.signal = self.conf.get("signal", [0])
        self.rfi = self.conf.get("rfi", [0])
        self.width = self.conf.get("width", 10000)
        self.fs = 0
        self.duration = 0
        self.reader = FrontendSimFile(self.dataset)
        self.antennas = self.reader.nelements
        self.pol = self.reader.npol

    @property
    def default(self) -> str:
        """Default process function
        """
        return "wideband"

    @property
    def shape(self) -> tuple:
        """The shape of the output array

        Returns:
            tuple: The shape
        """
        shape = []
        shape.append(self.reader.nelements)
        shape.append(self.reader.npol)
        shape.append(self.width)
        return tuple(shape)

    def wideband(self) -> np.ndarray:
        """Processing function to generate a wideband timeseries output for all PAF elements

        Returns:
            np.ndarray: The output array
        """
        # Read out signal and noise ACM from the reader object
        s_freq = self.reader.signal(self.signal, "frequencies")
        s_acmd = self.reader.signal(self.signal, "acm")
        n_acmd = self.reader.noise(self.noise, "acm")

        # Combine ACMs and retrieve frequency information
        self.input = np.asarray(s_acmd + n_acmd).sum(axis=0)
        fref = np.asarray(s_freq[0])*1e9
        nchan= len(fref)
        if self.width % nchan:
            print(f"Signal width {self.width} is not divisible by number of channels {nchan}")

        # Construct arrays
        channelized_array = np.zeros((nchan, self.antennas, self.width // nchan), dtype=np.complex128)
        out = np.zeros((self.antennas, self.pol, self.width), dtype=np.complex128)
        # Channelwise decomposition and signal generation
        for f in range(nchan):
            signal = (np.random.randn(self.antennas, self.width // nchan) + 1j\
                    * np.random.randn(self.antennas, self.width // nchan)) / np.sqrt(2)
            val, vec = np.linalg.eigh(self.input[f])
            for t in range(self.width//nchan):
                channelized_array[f, :, t] = (np.dot(np.dot(vec, np.sqrt(np.diag(val))),
                                                     signal[:, t])).flatten()
        # De-channelization
        for t in range(self.width//nchan):
            out[:, 0, t*nchan:(t+1)*nchan] = np.fft.ifft(channelized_array[:,:,t], axis=0).T
        return out

#pylint: disable=R0801
    def plot(self, path="", figsize=(8,4)):
        """Plotting function to plot the time series

        Args:
            path (str, optional): If not set to "" it stores the plot in the given directory. Defaults to "".
            figsize (tuple, optional): Size of the plotted figure. Defaults to (8,4).
        """
        sub = []
        fig = plt.figure(num = self.antennas, figsize=figsize)
        taxis = np.linspace(0, self.duration, self.shape[2])
        for a in range(self.antennas):
            for p in range(self.pol):
                sub.append(plt.subplot(self.antennas, self.pol, a*self.pol+p+1))
                sub[a*self.pol+p].set_title('Element: ' + str(a) + ', Pol:' + str(p))
                sub[a*self.pol+p].set_ylabel("Amplitude [V]")
                sub[a*self.pol+p].set_xlabel("Time [s]")
                sub[a*self.pol+p].plot(taxis, self.output[a])
        fig.tight_layout()
        if path:
            fig.savefig(path + self.name + '.png')
            plt.close()
        else:
            plt.show()
#pylint: enable=R0801
