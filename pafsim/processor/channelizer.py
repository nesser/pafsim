import numpy as np
import scipy.signal as ss
import matplotlib.pyplot as plt

from pafsim.processor._processor import Processor

class Channelizer(Processor):
    """The channelizer takes time series (voltages) and channelizes it.
    The input is a time series of the form APT
    The output is a channelized time series of the form FAPT
    """
    N_INPUT = 1
    I_FORMAT = [['A', 'P', 'T']]
    O_FORMAT = ['F', 'A', 'P', 'T']

    def __init__(self, name: str, **kwargs):
        """Construct a Generator object

        Args:
            name (str): The unique name of theGenerator
        kwargs:
            taps (int): Number of taps used for the filter. Defaults to 2
            channels (int): Number of channels to produce. Defaults to 32
            window (str): The window function to apply to the filter. The window function must be known
                by scipy.windows.get_window(). Defaults to rectangular
            pn (int): Numerater, used when oversampling is desired. Defaults to 1
            mode (str): pfb -> polyphase filterbank channelizer (default)
                        fft -> fourier transform channelizer
        """
        super().__init__(name, **kwargs)
        self.taps = kwargs.get('taps', 2)
        self.channels = kwargs.get('channels', 32)
        self.window = kwargs.get('window', "rectangular")
        self.pn = kwargs.get('pn', 1)
        self.pd = kwargs.get('pd', 1)

        # self.stop_min = kwargs.get('stop_min', 0.00001)
        self.coeff = np.ndarray
        self.os_factor = self.pn / self.pd
        self.tap_length = int(self.channels * 2)
        self.channel_bw = 1/self.channels
        self.os_nsamples = int(self.tap_length * self.os_factor) - self.tap_length


    @property
    def default(self) -> str:
        """Default process function
        """
        return "pfb"

    @property
    def shape(self) -> tuple:
        """The shape of the output array

        Returns:
            tuple: The shape
        """
        shape = []
        shape.append(self.channels)
        shape.append(self.pp[0].dim('A'))
        shape.append(self.pp[0].dim('P'))
        shape.append(int(self.pp[0].dim('T') * self.os_factor // (self.channels*2)))
        return tuple(shape)

    def _generateFilter(self) -> np.ndarray:
        """Generates the filter for the channelization.

        Returns:
            np.ndarray: The FIR filter
        """
        if self.mode == "pfb":
            self.coeff = ss.firwin(self.tap_length * self.taps,
                cutoff=self.pn/(self.pd*self.tap_length),
                window=self.window,
                scale=True).reshape(self.taps, self.tap_length).T
        elif self.mode == "fft":
            if np.iscomplexobj(self.pp[0].output):
                self.coeff = ss.windows.get_window(self.window, self.channels)
            else:
                self.coeff = ss.windows.get_window(self.window, self.channels*2)


    def _frontend(self, x: np.ndarray) -> np.ndarray:
        """Filter frontend for the pfb processing. Assumed to be a 'private' method

        Args:
            x (np.ndarray): input array

        Returns:
            np.ndarray: Output of the filter frontend
        """
        step = int(self.channels * 2 / self.os_factor)
        y = []
        offset = 0
        while True:
            start = offset * step
            end = start + self.taps * self.tap_length
            if end > x.size:
                break
            x_p = x[start:end].reshape(self.taps, self.tap_length).T
            summed = (x_p * self.coeff).sum(axis=1)
            y.append(summed)
            offset+=1
        return np.asarray(y)

    def pfb(self) -> np.ndarray:
        """Processing function to channelize the data with a Polyphase Filterbank

        Returns:
            np.ndarray: The output array
        """
        self._generateFilter()
        pad = 0
        out = []
        x = self.pp[0].output
        filter_length = self.tap_length * self.taps
        if x.shape[-1] < filter_length:
            pad = int(filter_length - x.shape[-1])
        elif x.shape[-1] % filter_length:
            pad = filter_length - x.shape[-1] % filter_length
        x = np.pad(x, [(0,0),(0,0),(0, pad)], 'constant')
        for a in range(self.dim('A')):
            out.append([])
            for p in range(self.dim('P')):
                x_fir = self._frontend(x[a,p])
                out[a].append(np.fft.rfft(x_fir, axis=1))
        return np.asarray(out).transpose(3,0,1,2)[:-1,...]

    def fft(self) -> np.ndarray:
        """Processing function to channelize the data with a FFT

        Returns:
            np.ndarray: The output array
        """
        self._generateFilter()
        if np.iscomplexobj(self.pp[0].output):
            reshaped = self.pp[0].output.reshape(self.dim('A'), self.dim('P'), -1, self.channels)
            return np.fft.fft(reshaped * self.coeff, axis=-1).transpose(3,0,1,2)
        reshaped = self.pp[0].output.reshape(self.dim('A'), self.dim('P'), -1, self.channels*2)
        return np.fft.rfft(reshaped * self.coeff, axis=-1).transpose(3,0,1,2)[1:]

#pylint: disable=R0801
    def plot(self, path="", figsize=(8,4)):
        """Plotting function to plot the channeles vs time

        Args:
            path (str, optional): If not set to "" it stores the plot in the given directory. Defaults to "".
            figsize (tuple, optional): Size of the plotted figure. Defaults to (8,4).
        """
        sub = []
        fig = plt.figure(num = (self.dim('A')+1)*self.dim('P'), figsize=figsize)
        for a in range(self.dim('A')):
            for p in range(self.dim('P')):
                sub.append(plt.subplot(self.dim('A')+1, self.dim('P'), a*self.dim('P')+p+1))
                sub[a*self.dim('P')+p].set_title('Element ' + str(a) + '.' + str(p))
                sub[a*self.dim('P')+p].set_xlabel("Time")
                sub[a*self.dim('P')+p].set_ylabel("Channel")
                sub[a*self.dim('P')+p].imshow(np.log(np.abs(self.output[:, a, p, :])),
                                              interpolation='nearest', aspect="auto")

        sub.append(plt.subplot(self.dim('A')+1, 1, self.dim('A')+1))
        sub[self.dim('A') * self.dim('P')].set_title("Filter function")
        sub[self.dim('A') * self.dim('P')].set_xlabel("Sample")
        sub[self.dim('A') * self.dim('P')].set_ylabel("Amplitude")
        sub[self.dim('A') * self.dim('P')].plot(np.arange(0,len(self.coeff)),
                                                self.coeff.flatten())
        fig.tight_layout()
        if path:
            fig.savefig(path + self.name + '.png')
            plt.close()
        else:
            plt.show()
#pylint: enable=R0801
