import numpy as np
from pafsim.processor._processor import Processor
from pafsim.processor.beamformer import Beamformer
from pafsim.processor.calibrator import WeightGenerator
from pafsim.processor.channelizer import Channelizer
from pafsim.processor.correlator import Correlator
from pafsim.processor.waveform import WaveformGenerator
from pafsim.processor.generator import Generator

def createProcessor(class_name: str, name: str, kwargs: dict) -> Processor:
    """Factory function to create a Processor by and name and configuration options

    Args:
        class_name (str): The class name of the Processor (e.g. Channelizer)
        name (str): The unique name of the processor
        kwargs (dict): The configuration options of the processor

    Returns:
        Processor: The created processor
    """
    return globals()[class_name](name, **kwargs)

def scale(data: np.ndarray, bits: int) -> np.ndarray:
    """Scales a numpy array to passed bit size

    Args:
        data (np.ndarray): The array to scale
        bits (int): The number of bits

    Returns:
        np.ndarray: The rescaled array
    """
    return (data / np.max(np.abs(data)) * 2**(bits - 1))

def rescalefloat2int(data: np.ndarray, dtype: np.dtype) -> np.ndarray:
    """Rescales a float array to an int array

    Args:
        data (np.ndarray): The float array to rescale
        dtype (np.dtype): The desired integer data type (e.g. np.int32)

    Returns:
        np.ndarray: The converted integer array
    """
    bits = np.dtype(dtype).itemsize * 8
    if np.max(data) > 2**(bits-1):
        data = scale(data, bits)
    return data.astype(dtype)

def complex2int2(data: np.ndarray, dtype: np.dtype) -> np.ndarray:
    """Rescales a float array to an int array

    Args:
        data (np.ndarray): The float array to rescale
        dtype (np.dtype): The desired integer data type (e.g. np.int32)

    Returns:
        np.ndarray: The converted integer array
    """
    bits = np.dtype(dtype).itemsize * 8
    dtype = np.dtype([('real', dtype), ('imag', dtype)])
    if np.max(data) > 2**(bits-1):
        data = scale(data, bits)
    return data.astype(dtype)


__all__ = [
    "Processor",
    "Beamformer",
    "WeightGenerator",
    "Channelizer",
    "Correlator",
    "WaveformGenerator",
    "Generator"
]
