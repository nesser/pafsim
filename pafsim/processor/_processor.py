from abc import ABC, abstractmethod
import os
import pickle
import numpy as np


class Processor(ABC):
    """The base class for all processors. It is the API for creating new Processor classes
    A Processor has to implement a shape-property and a plot function.
    """
    N_INPUT: int = 0
    I_FORMAT: list = []
    O_FORMAT: list = []

    def __init__(self, name: str, **kwargs):
        """Base constructor of the Processor. Can not be initiatied as this class is abstract

        Args:
            name (str): The unique name of theProcessor
        """
        self.pp: list[Processor] = [None for __ in range(self.N_INPUT)]
        self.name: str = name
        self.input: np.ndarray = np.zeros(0)
        self.output: np.ndarray = np.zeros(0)
        if 'kwargs' in kwargs:
            self.conf: dict = kwargs['kwargs']
        else:
            self.conf: dict = kwargs
        self.path: str = kwargs.get('path', '')
        self.mode: callable = kwargs.get('mode', self.default)
        self.dtype: np.dtype = np.dtype(kwargs.get('dtype', 'int8'))

    @property
    def default(self) -> str:
        """Returns the default mode of the specific processor.

        Returns:
            str: The default mode
        """
        return ""

    def process(self):
        """The process method calls the individual processing functions of the Processors.
            The actual processing function which should be executed is specified by the
            Processors mode

        Raises:
            TypeError: When no pre-processor is set
        """
        for pp in self.pp:
            if pp is None and self.mode != 'load':
                raise TypeError(f"Pre-processor for {self.name} is not set")
        self.output = getattr(self, self.mode)()
        # if self.output.shape != self.shape:
        #     raise ValueError(f"{self}/{self.name}: Shape mismatch, expected {self.shape}, but got {self.output.shape}")

    @property
    @abstractmethod
    def shape(self) -> tuple:
        """Abstract method should return the output array shape.
            Needs to be implemented by inherited classes

        Returns:
            tuple: The shape of the output array
        """

    @abstractmethod
    def plot(self, path: str="", figsize: tuple=(8,4)):
        """Abstract method to plot the output array.
            Needs to be implemented by inherited classes

        Args:
            path (str, optional): If set stores the plot in the directory. Defaults to "".
            figsize (tuple, optional): Te size of the figure to plot. Defaults to (8,4).
        """

    def dim(self, label: str) -> int:
        """Returns the size of the requested dimension

        Args:
            label (str): The label of the dimension (e.g. 'A', 'F', 'P')

        Raises:
            ValueError: When the processor has no dimension of the passed label

        Returns:
            int: The size of the dimension
        """
        try:
            idx = self.O_FORMAT.index(label)
        except ValueError:
            raise ValueError(f"Label {label} does not exists in {self.O_FORMAT}")
        return self.shape[idx]

    def setPreProcessor(self, processor):
        """Connects the processor with a pre-processor

        Args:
            processor (Processor): Concret object of the pre-processor
        """
        self.conf["input"] = processor.name
        self.pp[0] = processor

    @property
    def iformat(self) -> list:
        """The input format

        Returns:
            list: The input format as a list (label)
        """
        return self.I_FORMAT

    @property
    def oformat(self):
        """The output format

        Returns:
            list: The output format as a list (label)
        """
        return self.O_FORMAT

    def load(self) -> np.ndarray:
        """Method to load a pickled output dataset. This method is used by sub-classes as a 'mode'


        Raises:
            ValueError: When the output format does not match with the loaded dataset

        Returns:
            np.ndarray: The dataset which was loaded
        """
        with open(os.path.abspath(self.path), 'rb') as f:
            tv = pickle.load(f)
        if tv.order != self.O_FORMAT:
            raise ValueError(f"Loaded Vector has order {tv.order}, but expected {self.O_FORMAT}")
        return tv.data

def getDim(label: str, processor: Processor) -> int:
    """Returns the index position of the passed processor by its label

    Args:
        label (str):  The label of the dimension (e.g. 'A', 'F', 'P')
        processor (Processor): The processor

    Raises:
        ValueError: When the processor has no dimension of the passed label

    Returns:
        int: Index of the specific label
    """
    try:
        idx = processor.O_FORMAT.index(label)
    except ValueError:
        raise ValueError(f"Label {label} does not exists in {processor.O_FORMAT}")
    return processor.shape[idx]
