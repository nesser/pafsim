import numpy as np
import matplotlib.pyplot as plt

from pafsim.processor._processor import Processor
from pafsim.processor.channelizer import Channelizer
from pafsim.processor.calibrator import WeightGenerator


class Beamformer(Processor):
    """The Beamformer class forms beams out of voltage data and beam weights
        The Beamformer expects two inputs, the voltage data of the form of FPAT and the beam
        weights of the BFPA.
        The output is shape is FPAT
        The Beamformer inherits from the Processor class.
    """
    N_INPUT = 2
    I_FORMAT = [['F', 'P', 'A', 'T'], ['B', 'F', 'P', 'A']]
    O_FORMAT = ['F', 'B', 'P', 'T']

    def __init__(self, name: str, **kwargs):
        """Construct a Beamformer object

        Args:
            name (str): The unique name of the Beamformer
        """
        super().__init__(name, **kwargs)

    def setPreProcessor(self, processor: Processor):
        """Connects the processor with a pre-processor

        Args:
            processor (Processor): Concret object of the pre-processor

        Raises:
            TypeError: When the pre-processor is not of type WeightGenerator or Channelizer
        ToDo: The beamformer should allow any pre-processor with the matching dimensions.
        """
        if type(processor) in [WeightGenerator]:
            self.pp[1] = processor
        elif type(processor) in [Channelizer]:
            self.pp[0] = processor
        else:
            raise TypeError(f"Object {type(processor)} is not allowed as input for {type(self)}")

    @property
    def default(self) -> str:
        """Default process function
        """
        return "voltage"

    @property
    def shape(self) -> tuple:
        """The shape of the output array

        Returns:
            tuple: The shape
        """
        shape = []
        shape.append(int(self.pp[1].dim('F')))
        shape.append(int(self.pp[1].dim('B')))
        shape.append(int(self.pp[1].dim('P')))
        shape.append(int(self.pp[0].dim('T')))
        return tuple(shape)

    def voltage(self) -> np.ndarray:
        """Applys the beam weights to the voltage data and forms beams using a general matrix
        multiplication


        Returns:
            np.ndarray: The output array
        """
        if np.iscomplexobj(self.pp[0].output) or np.iscomplexobj(self.pp[1].output):
            i = self.pp[0].output.transpose(0,2,3,1)
            w = self.pp[1].output.transpose(1,2,3,0)
            return np.matmul(i, w.conj()).transpose(0,3,1,2)
        return np.dot(self.pp[0].output, self.pp[1].output.T)

#pylint: disable=R0801
    def plot(self, path="", figsize=(8,4)):
        """Plotting function to plot the beamformed time series

        Args:
            path (str, optional): If not set to "" it stores the plot in the given directory. Defaults to "".
            figsize (tuple, optional): Size of the plotted figure. Defaults to (8,4).
        """
        sub = []
        fig = plt.figure(num = self.dim('B') * self.dim('P'), figsize=figsize)
        for b in range(self.dim('B')):
            for p in range(self.dim('P')):
                sub.append(plt.subplot(self.dim('B'), self.dim('P'), b*self.dim('P')+p+1))
                sub[b*self.dim('P')+p].set_title('Beam ' + str(b) + '.' + str(p))
                sub[b*self.dim('P')+p].set_xlabel("Time")
                sub[b*self.dim('P')+p].set_ylabel("Channel")
                plt.imshow(np.log(np.abs(self.output[:, b, p, :])), interpolation='nearest', aspect='auto')
        fig.tight_layout()
        if path:
            fig.savefig(path + self.name + '.png')
            plt.close()
        else:
            plt.show()
#pylint: enable=R0801
