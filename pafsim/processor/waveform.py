from scipy import signal
import numpy as np
import matplotlib.pyplot as plt


from pafsim.processor import Processor

class WaveformGenerator(Processor):
    """The WaveformGenerator generates time series (voltages) of different waveforms (e.g. sine,
    random)."""
    N_INPUT = 0
    I_FORMAT = None
    O_FORMAT = ['A', 'P', 'T']

    def __init__(self, name:str, **kwargs):
        """Construct a WaveformGenerator object
        Args:
            name (str): The unique name of theGenerator
        kwargs:
            fs (float): The sampling rate at which to generate the time series [Hz]. Defaults to 1000 Hz
            duration (float): The duration of the signal [seconds]. Defaults to 1s
            gain (float): Gain factor to the amplitude of the signal. Defaults to 1
            freq (List[float]): When using the sinus mode generates sines at the frequencies [Hz]. defaults to fs / 4
            phase (float): When using the sinus mode adds a phase offset [rad]. Defaults to 0
            period (float): When using the sweep mode sets the sweep length in seconds. Defaults to duration
            f0 (float): When using the sweep mode sets the start frequency of the sweep [Hz]. Defaults to fs / 8
            f1 (float): When using the sweep mode sets the stop frequency of the sweep [Hz]. Defaults to fs / 4
            method (str): When using the sweep mode sets the sweep method. Defaults to 'linear'
            mean (float): When using the noise mode, sets the mean of the signal
            devitaion (float): When using the noise mode, sets the deviation of the signal from the mean
            antennas (int): The number of antennas to generate signals for. Defaults to 2
            pol (int): The number of polarization to generate signals for. Defaults to 2
            mode (str): The waveform mode,
                        sinus -> Generates addtive sine wave signals (default)
                        sweep -> Generates sweep signals
                        noise -> Generates noise signals
                        rect -> Generates rectangluar signals
        """
        super().__init__(name, **kwargs)
        self.fs = self.conf.get('fs',1e3)
        self.duration = self.conf.get('duration', 1)
        self.gain = self.conf.get('gain', 1.0)
        self.freq = self.conf.get('freq', self.fs / 4)
        self.phase = self.conf.get('phase', 0)
        self.offset = self.conf.get('offset', 0)
        self.period = self.conf.get('period', self.duration)
        self.mean = self.conf.get('mean', 0.0)
        self.deviation = self.conf.get('devition', 1.0)
        self.f0 = self.conf.get('f0', self.fs / 8)
        self.f1 = self.conf.get('f1', self.fs / 4)
        self.method = self.conf.get('method', 'linear')
        self.antennas = self.conf.get('antennas', 2)
        self.pol = self.conf.get('pol', 2)
        self.time = int(self.fs * self.duration)
        self.t = np.linspace(0, self.duration, self.time)#, endpoint=True, retstep=True))


    @property
    def default(self) -> str:
        """Default process function
        """
        return "sinus"

    @property
    def shape(self) -> tuple:
        """The shape of the output array

        Returns:
            tuple: The shape
        """
        shape = []
        shape.append(self.antennas)
        shape.append(self.pol)
        shape.append(self.time)
        return tuple(shape)

    def sinus(self)-> np.ndarray:
        """Processing function to generate addtive sine waves for a baseband signal

        Returns:
            np.ndarray: The output array
        """
        o = np.zeros(self.shape)
        for a in range(self.antennas):
            for p in range(self.pol):
                if isinstance(self.freq, list):
                    for f in self.freq:
                        o[a,p] += self.gain * np.sin(2 * np.pi * f * self.t + self.phase) + self.offset
                else:
                    o[a,p] = self.gain * np.sin(2 * np.pi * self.freq * self.t + self.phase) + self.offset
        return o

    def noise(self)-> np.ndarray:
        """Processing function to generate noise for a baseband signal

        Returns:
            np.ndarray: The output array
        """
        o = np.zeros(self.shape)
        for a in range(self.antennas):
            for p in range(self.pol):
                o[a,p] = self.gain * np.random.normal(self.mean, self.deviation, self.time) + self.offset
        return o

    def sweep(self)-> np.ndarray:
        """Processing function to generate sweeps for a baseband signal

        Returns:
            np.ndarray: The output array
        """
        return self.gain * signal.chirp(self.t, f0=self.f0, f1=self.f1, t1=self.period, method=self.method) + self.offset

    def rect(self) -> np.ndarray:
        """Processing function to generate rects for a baseband signal

        Returns:
            np.ndarray: The output array
        """
        o = np.zeros(self.shape)
        for a in range(self.antennas):
            for p in range(self.pol):
                o[a,p] = self.gain * np.where(self.t < self.period, 1, 0) + self.offset
        return o

    # def sawtooth(self):
    #     return self.gain * signal.sawtooth(2 * np.pi * self.period * self.t)

#pylint: disable=R0801
    def plot(self, path="", figsize=(8,4)):
        """Plotting function to plot the time series

        Args:
            path (str, optional): If not set to "" it stores the plot in the given directory. Defaults to "".
            figsize (tuple, optional): Size of the plotted figure. Defaults to (8,4).
        """
        sub = []
        fig = plt.figure(num = self.antennas, figsize=figsize)
        for a in range(self.antennas):
            for p in range(self.pol):
                sub.append(plt.subplot(self.antennas, self.pol, a*self.pol+p+1))
                sub[a*self.pol+p].set_title('Element: ' + str(a) + ', Pol:' + str(p))
                sub[a*self.pol+p].set_ylabel("Amplitude [V]")
                sub[a*self.pol+p].set_xlabel("Time [s]")
                sub[a*self.pol+p].plot(self.t, self.output[a,p])
        fig.tight_layout()
        if path:
            fig.savefig(path + self.name + '.png')
            plt.close()
        else:
            plt.show()
#pylint: enable=R0801
