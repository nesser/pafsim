import pickle
import numpy as np
from pafsim.processor._processor import Processor

class Vector():
    """Class to maintain datasets as vectors. Vector objects can be stored and loaded
        to use them in different simulation setups.
    """
    def __init__(self, processor: Processor=None):
        if processor is not None:
            self.setProcessor(processor)
            return
        self.name: str = ""
        self.data: np.ndarray = np.zeros(0)
        self.order: list = []
        self.dtype: np.dtype = np.int8
        self.dir:str = ""

    def setProcessor(self, processor: Processor):
        """Set the processor to gather its properties

        Args:
            processor (Processor): The processor
        """
        self.name = processor.name
        self.data = processor.output
        self.order = processor.oformat
        self.dtype = processor.dtype

    def __str__(self) -> str:
        """String representation of the Vector

        Returns:
            str: The string
        """
        s  = f"Vector of {self.name}"
        s += f"\n\tshape: {self.data.shape}"
        s += f"\n\torder: {self.order}"
        s += f"\n\tdtype: {self.dtype}"
        return s

    def store(self, path: str):
        """Store the vector in a specific directory

        Args:
            path (str): The location where to store the Vector
        """
        path = path + self.name + '.pk1'
        with open(path, 'wb') as f:
            pickle.dump(self, f, pickle.HIGHEST_PROTOCOL)

    def load(self, path: str):
        """Load a stored vector from disk

        Args:
            path (str): The location where the vector was stored
        """
        with open(path, 'rb') as f:
            loaded = pickle.load(f)
            self.__dict__.update(loaded.__dict__)
