import typing as ty
from collections import OrderedDict
import networkx as nx
from pafsim.chain import ProcessingChain
from pafsim.vector import Vector

class Executor():
    """The Executor enables the simulation of comprehensive simulation configuration with nested
        Processing chains.
    """
    def __init__(self, conf: dict=None):
        """Construct an Executor object

        Args:
            conf (dict, optional): The discrition of the simulation. Defaults to None.
        """
        self.test_vectors: ty.List[Vector] = []
        self.chains: ty.List[ProcessingChain] = []
        self.dag: nx.DiGraph = nx.DiGraph()
        self.conf: dict = conf
        self.input_vectors: ty.List[Vector] = []
        self.output_vectors: ty.List[Vector] = []

    def update(self, conf: dict):
        """Updates the current configuration

        Args:
            conf (dict): the dictionary to update with

        Raises:
            Exception: raises if the update fails
        """
        try:
            self.conf.update(conf)
        except Exception as e:
            raise Exception(f"Failed to update configuration file with {e}")

    def _sort_chains(self):
        """Used to sort the processing chains according to the dependencies speciefied in
            the configuration

        Raises:
            ValueError: _description_
        """
        for chain_name, chain_conf in self.conf.items():
            if "dependency" in chain_conf:
                if chain_conf["dependency"] not in self.conf:
                    raise ValueError(f"Processing chain {chain_name} depence on chain {chain_conf['dependency']} which does not exists in the configuration file")
                self.dag.add_edge(chain_conf["dependency"], chain_name)
        if not nx.is_directed_acyclic_graph(self.dag):
            raise ValueError("The graph is not a DAG")
        if self.dag.number_of_nodes() == 0:
            return
        self.conf = OrderedDict((node, self.conf[node]) for node in list(nx.topological_sort(self.dag)))

    def run(self, plot: bool=True, overwrite=True):
        """Starts with the execution of the defined processing components

        Args:
            plot (bool, optional): Plots the data of all components. Defaults to False.
        """
        if not isinstance(self.conf, dict):
            raise TypeError("Provided configuration is not a dictionary")
        self._sort_chains()
        for idx, (chain_name, conf) in enumerate(self.conf.items()):
            print(f"Creating ProcessingChain for simulation {chain_name}")
            self.chains.append(ProcessingChain(conf, chain_name))
            self.chains[idx].setup()
            self.chains[idx].process()
            self.chains[idx].save(plot, overwrite)

    def add(self, chain: ProcessingChain):
        """Adds a processing chain to the execution

        Args:
            chain (ProcessingChain): Another chain to execute
        """
        self.update(chain.conf)
        self.chains.append(chain)

    def getVectorById(self, name: str):
        """Get a test vector by its ID

        Args:
            name (str): The ID of a test vector

        Returns:
            Vector: The Vector
        """
        for i in self.input_vectors:
            if name == i.name:
                return i
        for i in self.output_vectors:
            if name == i.name:
                return i
        print(f"No Vector with name '{name}' found")
        return None

    def getChain(self, name: str) -> ProcessingChain:
        """Get a registered ProcessingChain object by its name

        Args:
            name (str): The name of the ProcessingChain

        Returns:
            ProcessingChain: The ProcessingChain object if it exists, otherwise None
        """
        for chain in self.chains:
            if chain.name == name:
                return chain
        return None
